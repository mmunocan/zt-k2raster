#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <list_files> <output_path>"
	exit
fi

list_files=$1
output_path=$2

mkdir -p $output_path
mkdir -p "$output_path/z_heu_10"
mkdir -p "$output_path/z_heu_11"

i=0
while IFS= read -r line
do
	set -- $line
		input_filename=$1
		input_path=$2
		raster_id=$3
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		nohup ./build/bin/ztk2_encode $input_filename $input_path "$output_path/z_heu_10/10_$raster_id.tk2r" 1 10 > "$output_path/z_heu_10/$output_result" &
		nohup ./build/bin/ztk2_encode $input_filename $input_path "$output_path/z_heu_11/11_$raster_id.tk2r" 1 11 > "$output_path/z_heu_11/$output_result" &
		
	i=$(($i + 1))
done < $list_files