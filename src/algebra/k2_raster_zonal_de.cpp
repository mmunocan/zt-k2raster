/*
 * Created by Fernando Silva on 28/10/21.
 *
 * Copyright (C) 2016-current-year, Fernando Silva, all rights reserved.
 *
 * Author's contact: Fernando Silva  <fernando.silva@udc.es>
 * Databases Lab, University of A Coruña. Campus de Elviña s/n. Spain
 *
 * Program to execute zonal operation between two k2-raster files
 *
 * k2-raster is a compact data structure to represent raster data that
 * uses compressed space and offers indexing capabilities.
 * It uses min/max values for indexing and improving query performance.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

// Own libraries
#include <k2_raster.hpp>
#include <utils/utils_time.hpp>
#include <utils/args/utils_args_algebra.hpp>


//**********************************************************************//
//************************** ALGEBRA ***********************************//
//**********************************************************************//
template<typename k2_raster_type,
        typename k2_raster_in_type = k2_raster_type,
        typename value_type=long,
        typename value_in_type=int>
void algebra_zonal(k2_raster_in_type &raster1, k2_raster_in_type &raster_zonal, const std::string &output_data,
                   const k2raster::OperationZonalRaster &operation, bool set_check) {
#ifndef NDEBUG
    // Check if there is am operation with that code
    std::cout << "Operation: ";
    switch (operation) {
        case k2raster::OperationZonalRaster::OPERATION_ZONAL_SUM:
            std::cout << "'Zonal Sum'" << std::endl;
            break;
        default:
            std::cout << "No valid operation << " << operation << std::endl;
            return;
    }
#endif

    /*********************/
    /* Run operation     */
    /*********************/
    auto t1 = util::time::user::now(); // Start time
    std::vector<value_in_type> raster_values, zonal_values;
    size_t n_rows1, n_cols1, n_rows2, n_cols2;
    raster1.decompress(raster_values, n_rows1, n_cols1);
    raster_zonal.decompress(zonal_values, n_rows2, n_cols2);

    // Calculate the value of each zone
    std::map<value_in_type, value_type> zonal_sum;
    {
        size_t p, pz;
        for (size_t r = 0; r < n_rows1; r++) {
            for (size_t c = 0; c < n_cols1; c++) {
                p = r * n_cols1 + c;
                pz = r * n_cols2 + c;
                if (zonal_sum.find(zonal_values[pz]) == zonal_sum.end()) {
                    zonal_sum[zonal_values[pz]] = raster_values[p];
                } else {
                    zonal_sum[zonal_values[pz]] += raster_values[p];
                }
            }
        }
        raster_values.clear();
        raster_values.shrink_to_fit();
    } // END BLOCK calculate zonal_sum (map)

    /*for (size_t p = 0; p < raster_values.size(); p++) {
        if (zonal_sum.find(zonal_values[p]) == zonal_sum.end()) {
            zonal_sum[zonal_values[p]] = raster_values[p];
        } else {
            zonal_sum[zonal_values[p]] += raster_values[p];
        }
        if (zonal_values[p] == 31) {
            std::cout << "zone: " << zonal_values[p] << " = " << zonal_sum[zonal_values[p]] << std::endl;
            std::cout << "(" << p / n_cols2 << ", " << p % n_cols2 << ")" << std::endl;
        }
    } // END FOR p*/


    // Create zonal result
    std::vector<value_type> result_values(n_rows2*n_cols2);
    size_t p = 0;
    for (auto & zonal_value : zonal_values) {
        result_values[p] = zonal_sum[zonal_value];
        p++;
    } // END FOR p
    zonal_values.clear();
    zonal_values.shrink_to_fit();


    k2_raster_type k2raster(result_values, n_rows2, n_cols2, raster1.k1, raster1.k2, raster1.level_k1, 0);
    auto t2 = util::time::user::now(); // End time

    // Print time/space
    auto time = util::time::duration_cast<util::time::milliseconds>(t2-t1);
    std::cout << "k2-raster build time: " << time;
    std::cout << " milliseconds." << std::endl;

    size_t k2_raster_size = sdsl::size_in_bytes(k2raster);
    double ratio = (k2_raster_size * 100.) / (k2raster.get_n_rows() * k2raster.get_n_cols() * sizeof(value_in_type));
    std::cout << "k2-rater space:" << k2_raster_size << " bytes (" << ratio << "%)" << std::endl;

    /*********************/
    /* Save structure    */
    /*********************/
    if (!output_data.empty()) {
#ifndef NDEBUG
        std::cout << std::endl << "Storing k2-raster structure in file: " << output_data << std::endl;
#endif
        sdsl::store_to_file(k2raster, output_data);
#ifndef NDEBUG
        std::string file_name = std::string(output_data) + ".html";
        sdsl::write_structure<sdsl::format_type::HTML_FORMAT>(k2raster, file_name);
#endif
    }

    /*********************/
    /* Check             */
    /*********************/
    if (set_check){
        std::cout << "Checking k2-raster........" << std::endl;
        uint n_rows = k2raster.get_n_rows();
        uint n_cols = k2raster.get_n_cols();

        for (size_t x = 0; x < n_rows; x++) {
            for (size_t y = 0; y < n_cols; y++) {
                value_type zone_value = zonal_sum[raster_zonal.get_cell(x, y)];
                value_type cell_value = k2raster.get_cell(x, y);
                if (zone_value != cell_value) {
                    std::cout << "Found error at position (" << x << ", " << y  << "), ";
                    std::cout << "expected " << zone_value << " and get " << cell_value << std::endl;
                    exit(-1);
                } // END IF result1
            } // END FOR y
        } // END FOR x
        std::cout << "ALL OK!!!!" << std::endl;
    } // END IF check
}


int main(int argc, char **argv) {

    /*********************/
    /* Reads params      */
    /*********************/
    args_algebra_zonal args;
    parse_args_algebra_zonal(argc, argv, args);

    /*********************/
    /* Run algebra       */
    /*********************/
    switch (args.k2_raster_type) {
        case k2raster::K2_RASTER_TYPE: {
            k2raster::k2_raster<int> raster1, rasterZ;

            // Load raster 1
            std::ifstream input_file_1(args.raster1);
            assert(input_file_1.is_open() && input_file_1.good());
            raster1.load(input_file_1);

            // Load raster 2
            std::ifstream input_file_zonal(args.raster_zonal);
            assert(input_file_zonal.is_open() && input_file_zonal.good());
            rasterZ.load(input_file_zonal);

            algebra_zonal<k2raster::k2_raster<long>, k2raster::k2_raster<int>, long, int>(raster1, rasterZ, args.output_data,
                                           static_cast<k2raster::OperationZonalRaster>(args.operation), args.set_check);
        }
            break;
        case k2raster::K2_RASTER_TYPE_PLAIN:
            break;
        case k2raster::K2_RASTER_TYPE_HEURISTIC:
            break;
        default:
            std::cout << "Invalid type " << args.k2_raster_type << ": " << std::endl;
            print_usage_algebra_zonal(argv);
            exit(-1);
    }

    return 0;
}