/* 
 * Código de Martita Muñoz 
 * Dado un Tk2-raster y su equivalente en DP, devuelve info relativa a los snapshots subyacentes de cada estructura
 */


#include <temporal/at_k2_raster.hpp>

using namespace std;

typedef sdsl::int_vector<1> bitmap_type;

void print_help(char * argv0) {
    printf("Usage: %s <tk2r_filename> <dp_filename> \n",
           argv0);
}

template<typename tk2_raster_type, typename k2_raster_snap_type>
void run(string tk2r_filename, string dp_filename){
	/******************************/
	/* LECTURA DE LAS ESTRUCTURAS */
	/******************************/
	tk2_raster_type tk2r;
	sdsl::load_from_file(tk2r, tk2r_filename);
	
	tk2_raster_type dp;
	sdsl::load_from_file(dp, dp_filename);
	
	vector<k2_raster_snap_type> tk2r_snaps = tk2r.get_snapshots();
	vector<k2_raster_snap_type> dp_snaps = dp.get_snapshots();
	
	/*****************************/
	/* MUESTRA DE LOS RESULTADOS */
	/*****************************/
	size_t tk2r_snaps_n = tk2r_snaps.size();
	size_t dp_snaps_n = dp_snaps.size();
	double tk2r_snaps_size = 0.0;
	double dp_snaps_size = 0.0;
	
	for(size_t i = 0; i < tk2r_snaps_n; i++){
		tk2r_snaps_size += sdsl::size_in_mega_bytes(tk2r_snaps[i]);
	}
	
	for(size_t i = 0; i < dp_snaps_n; i++){
		dp_snaps_size += sdsl::size_in_mega_bytes(dp_snaps[i]);
	}
	
	tk2r_snaps_size /= sdsl::size_in_mega_bytes(tk2r);
	dp_snaps_size /= sdsl::size_in_mega_bytes(dp);
	
	tk2r_snaps_size *= 100;
	dp_snaps_size *= 100;
	
	cout << std::setprecision(2) << std::fixed;
	cout << tk2r_filename << ";" << tk2r_snaps_size << ";" << dp_snaps_size << ";" << tk2r_snaps_n << ";" << dp_snaps_n << endl;
	
}

int main(int argc, char ** argv) {
	if(argc != 3){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string tk2r_filename = argv[1];
	string dp_filename = argv[2];
	
	/**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	ifstream k2raster_file(tk2r_filename);
	assert(k2raster_file.is_open() && k2raster_file.good());
	ushort k2_raster_type;
	sdsl::read_member(k2_raster_type, k2raster_file);
	k2raster_file.close();
	
	switch(k2_raster_type){
		case k2raster::AT_K2_RASTER_TYPE:
			run<k2raster::atk2r_type, k2raster::k2_raster<>>(tk2r_filename, dp_filename);
			break;
		case k2raster::ATH_K2_RASTER_TYPE:
			run<k2raster::athk2r_type, k2raster::k2_raster_heuristic<>>(tk2r_filename, dp_filename);
			break;
		default:
			cout << "El TK2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << endl;
			return -1;
	}
	
	return 0;
}