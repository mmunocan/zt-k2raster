/*
 * Código de Martita Muñoz 
 * Dado un ZTk2-raster y un archivo que contiene una lista de consultas, obtene el tiempo exacto que demora
 * la consulta getCell del ZTk2-raster. 

 */
#include <temporal/k2_raster_temporal.hpp>
#include <temporal/k2_raster_temporal_global.hpp>
#include <temporal/t_k2_raster.hpp>
#include <utils/query/query.hpp>
#include <utils/utils_time.hpp>

#include <temporal/at_k2_raster.hpp>
#include <temporal/z_k2_raster.hpp>

#define N_REPS 1

void print_help(char * argv0) {
    printf("Usage: %s <ztk2_raster_file> <query_file>\n", argv0);
}

long total_value = 0;

template<typename k2_raster_type, bool n_queries_beginning=true>
void run_queries(std::string zk2raster_filename, const std::string& query_filename, uint nreps) {

    /*********************/
    /* Read queries      */
    /*********************/
    std::vector<k2raster::query<true>> queries;

    std::ifstream query_file(query_filename);
    assert(query_file.is_open() && query_file.good());

    size_t n_queries = 0;
    if (n_queries_beginning) {
        query_file >> n_queries;
    }

    while (query_file.good() && (!n_queries_beginning || (queries.size() < n_queries))) {
		queries.emplace_back(query_file, true);
    }

    /*********************/
    /* Load structure    */
    /*********************/
	zk2raster<k2_raster_type> ztk2_raster;
	ztk2_raster.load_from_disk(zk2raster_filename);

    /*********************/
    /* Run queries       */
    /*********************/

    auto t1 = util::time::user::now(); // Start time
    
    for (uint r = 0; r < nreps; r++) {
        total_value = 0;
#ifndef NDEBUG
        size_t q = 0;
#endif
        for (const auto &query : queries) {
            int value = ztk2_raster.getCell(query.xini, query.yini, query.tini);
            total_value += value;
#ifndef NDEBUG
            std::cout << "Query " << q << " gets " << value << " ";
            query.print();
            q++;
#endif
        } // END FOR queries
    } // END FOR nreps
    auto t2 = util::time::user::now(); // End time

    { // Print Info
        auto time = util::time::duration_cast<util::time::milliseconds>(t2-t1);
		std::cout << zk2raster_filename << ";" << std::fixed << std::setprecision(10) << ((time * 1000.0)/((double)(nreps*queries.size()))) << std::endl;
    }
}

int main(int argc, char **argv) {

    if (argc != 3 && argc != 4) {
        print_help(argv[0]);
        exit(-1);
    }

    /*********************/
    /* Reads params      */
    /*********************/
    std::string ztk2_raster_file = argv[1];
    std::string query_filename = argv[2];
    uint nreps = argc == 4 ? atoi(argv[3]) : N_REPS;


    /*********************/
    /*k2-raster Type     */
    /*********************/
    // Load structure
    std::ifstream k2raster_file(ztk2_raster_file);
    assert(k2raster_file.is_open() && k2raster_file.good());

    ushort k2_raster_type;
    sdsl::read_member(k2_raster_type, k2raster_file);
    k2raster_file.close();


    /**********************************/
	/* COMPROBACION DEL TIPO CORRECTO */
	/**********************************/
	
	switch(k2_raster_type){
		case k2raster::K2_RASTER_TYPE:
			run_queries<k2raster::k2_raster<>>(ztk2_raster_file, query_filename, nreps);
		break;
		case k2raster::K2_RASTER_TYPE_PLAIN:
			run_queries<k2raster::k2_raster_plain<>>(ztk2_raster_file, query_filename, nreps);
		break;
		case k2raster::K2_RASTER_TYPE_HEURISTIC:
			run_queries<k2raster::k2_raster_heuristic<>>(ztk2_raster_file, query_filename, nreps);
		break;
		default:
			std::cout << "El K2raster tipo " << k2_raster_type << " no se puede procesar con nuestro programa" << std::endl;
			return -1;
	}
}
