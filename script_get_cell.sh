#!/usr/bin/bash

if (($# != 2));
then
	echo "Error! include <list_files> <output_path>"
	exit
fi

list_files=$1
output_path=$2

mkdir -p $output_path
mkdir -p "$output_path/getCell"

mkdir -p "$output_path/getCell/heu_112"
mkdir -p "$output_path/getCell/heu_111"
mkdir -p "$output_path/getCell/zt_10"
mkdir -p "$output_path/getCell/zt_11"

i=0
while IFS= read -r line
do
	set -- $line
		raster_id=$1
		cell_file=$2
		
		output_result="O_"
		if (($i < 10));
		then
			output_result+="00$i"
		elif (($i < 100));
		then
			output_result+="0$i"
		else
			output_result+="$i"
		fi
		
		./build/bin/get_cell_tk2r "$output_path/heu_111/111_$raster_id.tk2r" $cell_file > "$output_path/getCell/heu_111/$output_result"
		./build/bin/get_cell_tk2r "$output_path/heu_112/112_$raster_id.tk2r" $cell_file > "$output_path/getCell/heu_112/$output_result"
		./build/bin/ztk2_get_cell "$output_path/z_heu_10/10_$raster_id.tk2r" $cell_file > "$output_path/getCell/zt_10/$output_result"
		./build/bin/ztk2_get_cell "$output_path/z_heu_11/11_$raster_id.tk2r" $cell_file > "$output_path/getCell/zt_11/$output_result"
		
	i=$(($i + 1))
done < $list_files