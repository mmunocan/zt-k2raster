/*
 * Código de Martita Muñoz
 * Recibe un temporal raster y modifica cada celda multiplicándolo por un valor <factor>
 */

#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>

using namespace std;

void print_help(char * argv0) {
    printf("Usage: %s <inputs_filename> <input_path_folder> <factor> <outputs_filename> <outputs_path_folder> <outputs_rasters_initial_filename> \n",
           argv0);
}


int main(int argc, char ** argv){
	if(argc != 7){
	    print_help(argv[0]);
        exit(-1);
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string inputs_filename = argv[1];						// Archivo con lista de archivos de rasters temporales
	string inputs_path_folder = argv[2];					// Ubicación de los rasters temporales
	float factor = atof(argv[3]);							// Factor de multiplicación
	string outputs_filename = argv[4];						// Archivo de salida con lista de archivos de rasters temporales
	string outputs_path_folder = argv[5];					// Ubicación de salida de los rasters temporales
	string outputs_rasters_initial_filename = argv[6];		// Nombre inicial de los archivos de cada raster temporal
	
	/*******************************/
	/* LECTURA DE LISTA DE RASTERS */
	/*******************************/
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	inputs_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	
	ofstream outputs_file(outputs_filename);
	outputs_file << n_rows << " " << n_cols << " " << k1 << " " << k2 << " " << level_k1 << " " << plain_levels << endl;
	
	string raster_input_filename, raster_output_filename;
	int value;
	
	int time = 0;
	while(inputs_file >> raster_input_filename) {
		
		raster_input_filename = inputs_path_folder + "/" + raster_input_filename;
		
		raster_output_filename = outputs_rasters_initial_filename + "_" + to_string(time) + ".bin";
		outputs_file << raster_output_filename << endl;
		raster_output_filename = outputs_path_folder + "/" + raster_output_filename;
		
		ifstream raster_input_file(raster_input_filename);
		assert(raster_input_file.is_open() && raster_input_file.good());
		ofstream raster_output_file(raster_output_filename);
		
		for(size_t c = 0; c < (n_rows * n_cols); c++){
			raster_input_file.read((char *)(& value), sizeof(int));
			value *= factor;
			raster_output_file.write((char *)(& value), sizeof(int));
		}
		
		raster_input_file.close();
		raster_output_file.close();
		time++;
	}
	
	inputs_file.close();
	outputs_file.close();
	
	cout << "Dataset " << outputs_filename << " ready!" << endl;
	
	return 0;
}