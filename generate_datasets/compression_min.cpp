/*
 * Dado un conjunto de imagenes binarias, realiza el filtrado de los valores bajos, comprimiendolos a un solo valor
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <cassert>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 7){
		cout << "Use: " << argv[0] << " <inputs_filename> <inputs_path_folder> <outputs_filename> <outputs_path_folder> <outputs_rasters_initial_filename> <percentaje>" << endl;
		return -1;
	}
	
	/***********************************/
	/* RECEPCION PARAMETROS DE ENTRADA */
	/***********************************/
	string inputs_filename = argv[1];						// Archivo con lista de archivos de rasters temporales
	string inputs_path_folder = argv[2];					// Ubicación de los rasters temporales
	string outputs_filename = argv[3];						// Archivo de salida con lista de archivos de rasters temporales
	string outputs_path_folder = argv[4];					// Ubicación de salida de los rasters temporales
	string outputs_rasters_initial_filename = argv[5];		// Nombre inicial de los archivos de cada raster temporal
	int percentaje = atoi(argv[6]);							// Porcentaje de reducción
	
	/*******************************/
	/* LECTURA DE LISTA DE RASTERS */
	/*******************************/
	ifstream inputs_file(inputs_filename);
	assert(inputs_file.is_open() && inputs_file.good());
	
	size_t n_rows, n_cols, k1, k2, level_k1, plain_levels;
	inputs_file >> n_rows >> n_cols >> k1 >> k2 >> level_k1 >> plain_levels;
	string raster_input_filename;
	int value;
	int min_value = INT_MAX;
	int max_value = INT_MIN;
	
	vector<vector<int>> dataset;
	
	while(inputs_file >> raster_input_filename) {
		
		raster_input_filename = inputs_path_folder + "/" + raster_input_filename;
		ifstream raster_input_file(raster_input_filename);
		assert(raster_input_file.is_open() && raster_input_file.good());
		
		vector<int> raster_values;
		
		for(size_t c = 0; c < (n_rows * n_cols); c++){
			raster_input_file.read((char *)(& value), sizeof(int));
			if(value < min_value)	min_value = value;
			if(value > max_value)	max_value = value;
			raster_values.push_back(value);
		}
		
		raster_input_file.close();
		dataset.push_back(raster_values);
		
	}
	
	inputs_file.close();
	
	/***************************/
	/* APLICACION DEL FILTRADO */
	/***************************/
	int min_allowed = (((max_value - min_value) * percentaje)/100) + min_value;
	
	/**************************/
	/* ESCRITURA DE LA SALIDA */
	/**************************/
	ofstream outputs_file(outputs_filename);
	outputs_file << n_rows << " " << n_cols << " " << k1 << " " << k2 << " " << level_k1 << " " << plain_levels << endl;
	string raster_output_filename;
	
	for(size_t raster = 0; raster < dataset.size(); raster++){
		raster_output_filename = outputs_rasters_initial_filename + "_" + to_string(raster) + ".bin";
		outputs_file << raster_output_filename << endl;
		raster_output_filename = outputs_path_folder + "/" + raster_output_filename;
		ofstream raster_output_file(raster_output_filename);
		
		for(size_t c = 0; c < (n_rows * n_cols); c++){
			if(dataset[raster][c] < min_allowed){
				value = min_value;
			}else{
				value = dataset[raster][c];
			}
			raster_output_file.write((char *)(& value), sizeof(int));
		}
		raster_output_file.close();
	}
	outputs_file.close();
	
	cout << "Dataset " << outputs_filename << " ready!" << endl;
	
	return 0;
}