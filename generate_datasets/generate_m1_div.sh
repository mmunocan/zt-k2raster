#!/usr/bin/bash

mkdir "../../dataset/temporal/m1lsm_div"

g++ mult_factor.cpp -o mult_factor

./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH0.txt ../../dataset/temporal/m1lsm/ 0.1 ../../dataset/temporal/m1lsm_div/m1lsmCH0DIV10.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH0DIV10 
./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH1.txt ../../dataset/temporal/m1lsm/ 0.1 ../../dataset/temporal/m1lsm_div/m1lsmCH1DIV10.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH1DIV10 
./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH2.txt ../../dataset/temporal/m1lsm/ 0.1 ../../dataset/temporal/m1lsm_div/m1lsmCH2DIV10.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH2DIV10 
./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH3.txt ../../dataset/temporal/m1lsm/ 0.1 ../../dataset/temporal/m1lsm_div/m1lsmCH3DIV10.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH3DIV10 
./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH4.txt ../../dataset/temporal/m1lsm/ 0.1 ../../dataset/temporal/m1lsm_div/m1lsmCH4DIV10.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH4DIV10 

./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH0.txt ../../dataset/temporal/m1lsm/ 0.01 ../../dataset/temporal/m1lsm_div/m1lsmCH0DIV100.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH0DIV100 
./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH1.txt ../../dataset/temporal/m1lsm/ 0.01 ../../dataset/temporal/m1lsm_div/m1lsmCH1DIV100.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH1DIV100
./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH2.txt ../../dataset/temporal/m1lsm/ 0.01 ../../dataset/temporal/m1lsm_div/m1lsmCH2DIV100.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH2DIV100 
./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH3.txt ../../dataset/temporal/m1lsm/ 0.01 ../../dataset/temporal/m1lsm_div/m1lsmCH3DIV100.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH3DIV100 
./mult_factor ../../dataset/temporal/m1lsm/m1lsmCH4.txt ../../dataset/temporal/m1lsm/ 0.01 ../../dataset/temporal/m1lsm_div/m1lsmCH4DIV100.txt ../../dataset/temporal/m1lsm_div/ m1lsmCH4DIV100 