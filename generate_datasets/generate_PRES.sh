#!/usr/bin/bash

mkdir "../../dataset/temporal/PRES"

g++ mult_factor.cpp -o mult_factor

./mult_factor  ../../dataset/temporal/NASA/PRES_NLDAS_FORA0125_H.txt  ../../dataset/temporal/NASA/ 0.1  ../../dataset/temporal/PRES/PRES_01.txt  ../../dataset/temporal/PRES/ PRES_01 
./mult_factor  ../../dataset/temporal/NASA/PRES_NLDAS_FORA0125_H.txt  ../../dataset/temporal/NASA/ 0.01  ../../dataset/temporal/PRES/PRES_001.txt  ../../dataset/temporal/PRES/ PRES_001 
./mult_factor  ../../dataset/temporal/NASA/PRES_NLDAS_FORA0125_H.txt  ../../dataset/temporal/NASA/ 0.001  ../../dataset/temporal/PRES/PRES_0001.txt  ../../dataset/temporal/PRES/ PRES_0001 
./mult_factor  ../../dataset/temporal/NASA/PRES_NLDAS_FORA0125_H.txt  ../../dataset/temporal/NASA/ 0.0001  ../../dataset/temporal/PRES/PRES_00001.txt  ../../dataset/temporal/PRES/ PRES_00001 
./mult_factor  ../../dataset/temporal/NASA/PRES_NLDAS_FORA0125_H.txt  ../../dataset/temporal/NASA/ 0.00001  ../../dataset/temporal/PRES/PRES_000001.txt  ../../dataset/temporal/PRES/ PRES_000001 
./mult_factor  ../../dataset/temporal/NASA/PRES_NLDAS_FORA0125_H.txt  ../../dataset/temporal/NASA/ 0.000001  ../../dataset/temporal/PRES/PRES_0000001.txt  ../../dataset/temporal/PRES/ PRES_0000001 