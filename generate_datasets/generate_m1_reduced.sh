#!/usr/bin/bash

mkdir "../../dataset/temporal/m1lsm_reduced"

g++ compression_min.cpp -o compression_min

./compression_min ../../dataset/temporal/m1lsm/m1lsmCH0.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH0RED01.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH0RED01ID 1
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH1.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH1RED01.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH1RED01ID 1
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH2.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH2RED01.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH2RED01ID 1
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH3.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH3RED01.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH3RED01ID 1
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH4.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH4RED01.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH4RED01ID 1

./compression_min ../../dataset/temporal/m1lsm/m1lsmCH0.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH0RED02.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH0RED02ID 2
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH1.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH1RED02.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH1RED02ID 2
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH2.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH2RED02.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH2RED02ID 2
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH3.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH3RED02.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH3RED02ID 2
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH4.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH4RED02.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH4RED02ID 2

./compression_min ../../dataset/temporal/m1lsm/m1lsmCH0.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH0RED05.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH0RED05ID 5
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH1.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH1RED05.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH1RED05ID 5
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH2.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH2RED05.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH2RED05ID 5
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH3.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH3RED05.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH3RED05ID 5
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH4.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH4RED05.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH4RED05ID 5

./compression_min ../../dataset/temporal/m1lsm/m1lsmCH0.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH0RED10.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH0RED10ID 10
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH1.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH1RED10.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH1RED10ID 10
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH2.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH2RED10.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH2RED10ID 10
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH3.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH3RED10.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH3RED10ID 10
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH4.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH4RED10.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH4RED10ID 10

./compression_min ../../dataset/temporal/m1lsm/m1lsmCH0.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH0RED15.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH0RED15ID 15
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH1.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH1RED15.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH1RED15ID 15
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH2.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH2RED15.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH2RED15ID 15
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH3.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH3RED15.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH3RED15ID 15
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH4.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH4RED15.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH4RED15ID 15

./compression_min ../../dataset/temporal/m1lsm/m1lsmCH0.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH0RED20.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH0RED20ID 20
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH1.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH1RED20.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH1RED20ID 20
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH2.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH2RED20.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH2RED20ID 20
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH3.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH3RED20.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH3RED20ID 20
./compression_min ../../dataset/temporal/m1lsm/m1lsmCH4.txt ../../dataset/temporal/m1lsm/ ../../dataset/temporal/m1lsm_reduced/m1lsmCH4RED20.txt ../../dataset/temporal/m1lsm_reduced/ m1lsmCH4RED20ID 20
