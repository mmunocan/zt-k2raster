'''
	Dado algunos parametros, genera un dataset sintetico con distribucion zipf
'''
import sys
import numpy as np

if len(sys.argv) != 9:
	print("ERROR! USE " + sys.argv[0] + " <outputs_filename> <outputs_path> <n_elements> <n_rows> <n_cols> <outputs_rasters_initial_filename>   <alpha_espacial> <alpha_temporal> ")
	sys.exit()

outputs_filename = sys.argv[1]
outputs_path = sys.argv[2]
n_elements = int(sys.argv[3])
n_rows = int(sys.argv[4])
n_cols = int(sys.argv[5])
outputs_rasters_initial_filename = sys.argv[6]
alpha_espacial = int(sys.argv[7])
alpha_temporal = int(sys.argv[8])

values = np.zeros((n_elements, n_rows, n_cols), dtype=np.int32)
values[0] = np.random.zipf(a=alpha_espacial, size=(n_rows, n_cols))

for i in range(1, n_elements):
	values[i] = values[i-1] + np.random.zipf(a=alpha_temporal, size=(n_rows, n_cols)) - np.ones((n_rows, n_cols), dtype=np.int32)
	

output_file = open(outputs_filename, "w")
first_line = str(n_rows)+" "+str(n_cols)+" 4 2 3 2"
output_file.write(first_line+"\n")
for i in range(n_elements):
	output_raster_filename = outputs_rasters_initial_filename + "_" + str(i) + ".bin"
	output_file.write(output_raster_filename+"\n")
	output_raster_filename = outputs_path + "/" + output_raster_filename
	
	raster_output = open(output_raster_filename, "wb")
	values[i].tofile(raster_output)
	raster_output.close()
output_file.close()

print("dataset " + outputs_filename + " ready!")