#!/usr/bin/bash

mkdir "../../dataset/temporal/zipf"

for ((E=0;E<5;E++));
do
	# Modificando el alpha temporal
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_at_2_$E.txt  ../../dataset/temporal/zipf/ 2048 512 512 zipf_at_2_$E_ 10 2
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_at_5_$E.txt  ../../dataset/temporal/zipf/ 2048 512 512 zipf_at_5_$E_ 10 5
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_at_10_$E.txt  ../../dataset/temporal/zipf/ 2048 512 512 zipf_at_10_$E_ 10 10
	
	# Modificando el alpha espacial
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_ae_2_$E.txt  ../../dataset/temporal/zipf/  2048 512 512 zipf_ae_2_$E_ 2 10
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_ae_5_$E.txt  ../../dataset/temporal/zipf/  2048 512 512 zipf_ae_5_$E_ 5 10
	
	# Modificando el tamaño de los rasters
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_r_256_$E.txt  ../../dataset/temporal/zipf/  2048 256 256 zipf_r_256_$E_ 10 10
	
	# Modificando la cantidad de rasters
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_tr_128_$E.txt  ../../dataset/temporal/zipf/  128 512 512 zipf_tr_128_$E_ 10 10
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_tr_256_$E.txt  ../../dataset/temporal/zipf/  256 512 512 zipf_tr_256_$E_ 10 10
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_tr_512_$E.txt  ../../dataset/temporal/zipf/  512 512 512 zipf_tr_512_$E_ 10 10
	python3 generate_zipf.py  ../../dataset/temporal/zipf/zipf_tr_1024_$E.txt  ../../dataset/temporal/zipf/  1024 512 512 zipf_tr_1024_$E_ 10 10
done