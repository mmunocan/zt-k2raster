#!/usr/bin/bash

g++ generate_query_get_cell.cpp -o generate_query_get_cell

./generate_query_get_cell 224 464 5 188 > ../../dataset/temporal/queries_NASA/cell_APCP_0005_NLDAS_FORA0125_H.txt
./generate_query_get_cell 224 464 10 375 > ../../dataset/temporal/queries_NASA/cell_APCP_0010_NLDAS_FORA0125_H.txt
./generate_query_get_cell 224 464 15 563 > ../../dataset/temporal/queries_NASA/cell_APCP_0015_NLDAS_FORA0125_H.txt
./generate_query_get_cell 224 464 50 1877 > ../../dataset/temporal/queries_NASA/cell_APCP_0050_NLDAS_FORA0125_H.txt
./generate_query_get_cell 224 464 100 3754 > ../../dataset/temporal/queries_NASA/cell_APCP_0100_NLDAS_FORA0125_H.txt
./generate_query_get_cell 224 464 500 18769 > ../../dataset/temporal/queries_NASA/cell_APCP_0500_NLDAS_FORA0125_H.txt
./generate_query_get_cell 224 464 1000 37538 > ../../dataset/temporal/queries_NASA/cell_APCP_1000_NLDAS_FORA0125_H.txt
./generate_query_get_cell 224 464 1500 56306 > ../../dataset/temporal/queries_NASA/cell_APCP_1500_NLDAS_FORA0125_H.txt

mkdir -p ../../dataset/temporal/queries_m1lsm

./generate_query_get_cell 752 752 301 100000 > ../../dataset/temporal/queries_m1lsm/cell_m1lsmCH0.txt
./generate_query_get_cell 752 752 301 100000 > ../../dataset/temporal/queries_m1lsm/cell_m1lsmCH1.txt
./generate_query_get_cell 752 752 301 100000 > ../../dataset/temporal/queries_m1lsm/cell_m1lsmCH2.txt
./generate_query_get_cell 752 752 301 100000 > ../../dataset/temporal/queries_m1lsm/cell_m1lsmCH3.txt
./generate_query_get_cell 752 752 301 100000 > ../../dataset/temporal/queries_m1lsm/cell_m1lsmCH4.txt

mkdir -p ../../dataset/temporal/queries_zipf

./generate_query_get_cell 512 512 2048 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_at_2.txt
./generate_query_get_cell 512 512 2048 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_at_5.txt
./generate_query_get_cell 512 512 2048 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_at_10.txt
./generate_query_get_cell 512 512 2048 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_ae_2.txt
./generate_query_get_cell 512 512 2048 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_ae_5.txt
./generate_query_get_cell 512 512 2048 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_r_256.txt
./generate_query_get_cell 512 512 128 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_tr_128.txt
./generate_query_get_cell 512 512 256 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_tr_256.txt
./generate_query_get_cell 512 512 512 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_tr_512.txt
./generate_query_get_cell 512 512 1024 100000 > ../../dataset/temporal/queries_zipf/cell_zipf_tr_1024.txt

