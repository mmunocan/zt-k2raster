#include <iostream>
#include <random>

using namespace std;

int main(int argc, char ** argv){
	if(argc != 5){
		cerr << "usage: " << argv[0] << " <n_rows> <n_cols> <n_rast> <cant> " << endl;
		return -1;
	}
	
	size_t n_rows = atoi(argv[1]);
	size_t n_cols = atoi(argv[2]);
	size_t n_rast = atoi(argv[3]);
	size_t cant = atoi(argv[4]);
	
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<> rows_distr(0, n_rows-1);
	uniform_int_distribution<> cols_distr(0, n_cols-1);
	uniform_int_distribution<> rast_distr(0, n_rast-1);
	
	cout << cant << endl;
	for(size_t n = 0; n < cant; n++){
		cout << rows_distr(gen) << ' ' << cols_distr(gen) << ' ' << rast_distr(gen) << endl;
	}
	return 0;
}